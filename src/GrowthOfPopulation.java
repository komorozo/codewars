public class GrowthOfPopulation {

    public static void main(String[] args) {
nbYear(1500,5,100,5000);
    }

    public static int nbYear(int p0, double percent, int aug, int p) {
        int wynik = p0;
        int years = 0;
        double procent = percent / 100;
        double suma = wynik;
        while (suma <= p) {
            suma = suma + suma * procent + aug;
            years++;
        }
        System.out.println(years);
        return years;
    }
}