import com.sun.deploy.util.ArrayUtil;

import java.util.Arrays;

public class ReversedString {

    public static void main(String[] args) {
        solution("taka smieciara malo inteligentna");
    }
    public static String solution(String str) {
        char[] charArray = new char[str.length()];
        String finalString = "";

        for (int i = str.length()-1; i >=0 ; i--) {
         charArray[i] = str.charAt(i);
         finalString = finalString + str.charAt(i);
        }


        System.out.println(finalString);
        return finalString;
    }

}
