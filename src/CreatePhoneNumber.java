public class CreatePhoneNumber {

    public static void main(String[] args) {
        createPhoneNumber(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});

    }

    public static String createPhoneNumber(int[] numbers) {

        String firstPart = "";
        String secondPart = "";
        String thirdPart = "";

        for (int i = 0; i <3 ; i++) {
           firstPart = firstPart + String.valueOf(numbers[i]);
        }
        for (int i = 3; i <6 ; i++) {
            secondPart = secondPart + String.valueOf(numbers[i]);
        }
        for (int i = 6; i <numbers.length ; i++) {
            thirdPart = thirdPart + String.valueOf(numbers[i]);
        }
        String output = String.format("(%s) %s-%s",firstPart,secondPart,thirdPart);
        return output;
    }

}
