import java.util.Map;
import java.util.TreeMap;

public class EasyWallpaper {

    public static void main(String[] args) {
        wallpaper(4, 3.5, 3);
        wallpaper(6.3, 4.5, 3.29);
        wallpaper(6.3, 5.8, 3.13);


    }

    public static String wallpaper(double l, double w, double h) {
        Map<Integer, String> mapOfResults = new TreeMap<>();
        mapOfResults.put(0, "zero");
        mapOfResults.put(1, "one");
        mapOfResults.put(2, "two");
        mapOfResults.put(3, "three");
        mapOfResults.put(4, "four");
        mapOfResults.put(5, "five");
        mapOfResults.put(6, "six");
        mapOfResults.put(7, "seven");
        mapOfResults.put(8, "eight");
        mapOfResults.put(9, "nine");
        mapOfResults.put(10, "ten");
        mapOfResults.put(11, "eleven");
        mapOfResults.put(12, "twelve");
        mapOfResults.put(13, "thirteen");
        mapOfResults.put(14, "fourteen");
        mapOfResults.put(15, "fifteen");
        mapOfResults.put(16, "sixteen");
        mapOfResults.put(17, "seventeen");
        mapOfResults.put(18, "eighteen");
        mapOfResults.put(19, "nineteen");
        mapOfResults.put(20, "twenty");

        if (l == 0 || w == 0 || h == 0) {
            return "zero";
        }else

        {
            double wallArea = (l * h * 2) + (w * h * 2);
            double rollArea = 5.2;
            double percent = (wallArea) * 0.15;
            double countOfRolls = (wallArea + percent) / rollArea;
            int round = (int) Math.ceil(countOfRolls);
            String output = mapOfResults.get(round);
            System.out.println(output);


            return output;
        }
    }
}
