import java.util.Set;
import java.util.TreeSet;

public class TwoToOne {

    public static void main(String[] args) {
        longest("aretheyhere", "yestheyarehere");
        System.out.println();
        longest("loopingisfunbutdangerous", "lessdangerousthancoding");
        longest("inmanylanguages", "theresapairoffunctions");
        System.out.println();

    }


    public static String longest(String s1, String s2) {
        Set<Character> charSet = new TreeSet<>();

        String longest = "";
        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);
            charSet.add(c);
        }
        for (int i = 0; i < s2.length(); i++) {
            char c = s2.charAt(i);
            charSet.add(c);
        }
        for (Character character : charSet){
            longest = longest + character;
        }
        System.out.println(longest);
        return longest;
    }


}
