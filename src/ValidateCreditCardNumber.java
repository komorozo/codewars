public class ValidateCreditCardNumber {

    public static void main(String[] args) {
        validate("891");
    }

    public static boolean validate(String n) {
        boolean validate = false;
        int intToValidate =0;
        int[] stringToArray = new int[n.length()];
        fillArray(stringToArray, n);
        boolean evenOdd = evenOrOdd(stringToArray);

        if (evenOdd == true) {
         intToValidate = countCodeForEven(stringToArray);
        }else {
            intToValidate = countCodeForOdd(stringToArray);
        }
        validate = isValidate(intToValidate);


        System.out.println(intToValidate);
        System.out.println(validate);
        return validate;
    }

    private static boolean isValidate(int intToValidate) {
        if (intToValidate%10 == 0){
            return  true;
        }else
        return false;
    }


    private static int countCodeForOdd(int[] stringToArray) {
        int sum = 0;
        for (int i = 1; i < stringToArray.length; i = i + 2) {
            stringToArray[i] = stringToArray[i] * 2;
        }
        for (int i = 0; i < stringToArray.length; i++) {
            if (stringToArray[i] > 9) {
                stringToArray[i] = sumDigits(stringToArray[i]);
            }
            sum = sum + stringToArray[i];
        }
        return sum;
    }

    private static int[] fillArray(int[] stringToArray, String n) {
        for (int i = 0; i < stringToArray.length; i++) {
            stringToArray[i] = Integer.parseInt(n.substring(i, i + 1));
        }
        return stringToArray;
    }

    private static int countCodeForEven(int[] stringToArray) {
        int sum = 0;
        for (int i = 0; i < stringToArray.length; i = i + 2) {
            stringToArray[i] = stringToArray[i] * 2;
        }
        for (int i = 0; i < stringToArray.length; i++) {
            if (stringToArray[i] > 9) {
                stringToArray[i] = sumDigits(stringToArray[i]);
            }
            sum = sum + stringToArray[i];
        }
      return sum;
    }

    private static int sumDigits(int i) {

        int resultOfSum = 0;
        while (i > 0) {
            resultOfSum = resultOfSum + i % 10;
            i = i / 10;
        }
        i = resultOfSum;
        return i;
    }

    private static boolean evenOrOdd(int[] stringToArray) {
        if (stringToArray.length % 2 == 0) {
            //if true its even
            return true;

        } else
            //if false its odd
            return false;
    }
}
